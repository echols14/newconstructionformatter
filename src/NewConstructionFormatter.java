import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewConstructionFormatter {
	private static final boolean SORT_CONSTRUCTIONS = false;

	public static void main(String[] args){
		//get the command-line arguments
		if(args.length < 2){
			System.out.println("Insufficient arguments.");
			return;
		}
		String fileNameIn = args[0];
		String fileNameOut = args[1];

		try {
			//read the whole input file
			BufferedReader fileIn = new BufferedReader(new InputStreamReader(new FileInputStream(new File(fileNameIn)), StandardCharsets.UTF_8));
			StringBuilder builder = new StringBuilder();
			while(fileIn.ready()){
				builder.append(fileIn.readLine());
				builder.append('\n');
			}
			//close the input file
			fileIn.close();

			//create a regex to capture what we're looking for
			Pattern pattern = Pattern.compile("\\b(.+?)\\b.*?\\(.*?\"(.+?)\".*?\\)");
			//open the output file
			BufferedWriter fileOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(fileNameOut)), StandardCharsets.UTF_8));
			//look for matches and write them to the output
			Matcher matcher = pattern.matcher(builder.toString());
			List<NewConstruction> list = new LinkedList<>();
			while(matcher.find()){
				MatchResult result = matcher.toMatchResult();
				NewConstruction construction = new NewConstruction(result.group(1), result.group(2));
				list.add(construction);
			}
			if(SORT_CONSTRUCTIONS) list.sort((newConstruction, t1) -> newConstruction.getName().compareToIgnoreCase(t1.getName()));
			for(NewConstruction construction: list){
				fileOut.write(construction.toString());
			}
			//close the output file
			fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static class NewConstruction {
		//members
		private String name;
		private String description;
		//constructor
		public NewConstruction(String name, String description) {
			this.name = name;
			this.description = description;
		}
		//getters
		public String getName() {
			return name;
		}
		public String getDescription() {
			return description;
		}

		@Override
		public String toString() {
			return "_gram-name_\t" + name + "\t<" + description + ">\n" +
					"_gram-path_\t" + name + "\t<" + description + "_PATH>\n" +
					"_gram-helpText_\t" + name + "\t<" + description + "_HELP_TEXT>\n";
		}
	}
}
